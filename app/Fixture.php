<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fixture extends Model {

    protected $fillable = ['fixture_id', 'user_id', 'league_id','results'];

    protected $hidden = ['id', 'user_id', 'league_id', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function league()
    {
        return $this->hasOne('App\League');
    }
}
