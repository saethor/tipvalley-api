<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class League extends Model {

    protected $fillable = ['name', 'description'];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function fixtures()
    {
        return $this->hasMany('App\Fixture');
    }

    public function seasons()
    {

        return $this->join('league_season', function ( $join )
        {
            $join->on('leagues.id', '=', 'league_season.league_id')->where('league_id', '=', $this->id);
        })->get();
    }

    public function addSeason( $id )
    {
        DB::table('league_season')->insert(['season_id' => $id, 'league_id' => $this->id, 'created_at' => (new \DateTime())->format('Y-m-d H:m:s'), 'updated_at' => (new \DateTime())->format('Y-m-d H:m:s')]);
    }

    public function removeSeason( $id )
    {
        DB::table('league_season')->where('season_id', $id)->where('league_id', $this->id)->delete();
    }
}
