<?php


namespace App\Tipvalley\Repositories\League;


use App\League;
use App\Tipvalley\Exceptions\LeagueNotFoundException;
use App\Tipvalley\Exceptions\SeasonAlreadyExistsException;
use App\Tipvalley\Exceptions\SeasonNotExistsException;
use App\Tipvalley\Exceptions\UserInLeagueException;
use App\Tipvalley\Exceptions\UserNotFoundException;
use App\Tipvalley\Exceptions\UserNotInLeagueException;
use App\Tipvalley\Repositories\EloquentRepository;
use App\Tipvalley\Repositories\User\UserRepository;


class EloquentLeagueRepository extends EloquentRepository implements LeagueRepository {

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * DbLeagueRepository constructor.
     *
     * @param UserRepository $userRepository
     * @param League $model
     */
    public function __construct( UserRepository $userRepository, League $model )
    {
        parent::__construct($model);
        $this->model = $model;
        $this->userRepository = $userRepository;
    }

    /**
     * Returns all the leagues
     *
     * @return array
     * @throws \Exception
     */
    public function getAll()
    {
        $response = parent::getAll();

        if ( $response->isEmpty() )
            throw new LeagueNotFoundException('Sorry, no league was found');

        return $response->toArray();
    }

    /**
     * Returns a single League instance
     *
     * @param $id
     * @return League
     * @throws \Exception
     */
    public function getById( $id )
    {
        $response = parent::getById($id);

        if ( ! $response )
            throw new LeagueNotFoundException('Sorry, the league you were looking for does not exists');

        return $response;
    }

    /**
     * Returns all the users for a single league
     *
     * @param $leagueId
     * @return array
     * @throws \Exception
     */
    public function getUsers( $leagueId )
    {
        $league = $this->getById($leagueId);
        $users = $league->users;

        if ( $users->isEmpty() )
            throw new \Exception('This league has no users');

        return $users->toArray();
    }

    /**
     * Creates a new league and makes the user who makes it a admin
     *
     * @param $name
     * @param $description
     * @param $seasonId
     * @param array $players
     * @return array
     */
    public function newLeague( $name, $description, $seasonId, array $players = null )
    {
        $user = $this->userRepository->getUser();
        $league = $this->model->create([
            'name'        => $name,
            'description' => $description
        ]);

        $league->addSeason($seasonId);

        $league->users()->attach($user, ['admin' => 1]);

        foreach ( $players as $player )
        {
            try
            {
                $user = $this->userRepository->getByEmail($player);
                $league->users()->attach($user['id']);
            } catch ( UserNotFoundException $e )
            {

            }

        }

        return "{$league->name} was successfully created";
    }

    /**
     * Updates a single league
     *
     * @param $leagueId
     * @param $name
     * @param $description
     * @return string
     * @throws \Exception
     */
    public function updateLeague( $leagueId, $name, $description )
    {
        $league = $this->model->find($leagueId);
        if ( ! $league )
            throw new LeagueNotFoundException('Sorry, could not update that league');

        $league->name = $name;
        $league->description = $description;
        $league->save();

        return "{$league->name} was successfully updated";
    }

    /**
     * Deletes a league
     *
     * @param $leagueId
     * @return string
     */
    public function deleteLeague( $leagueId )
    {
        $league = $this->model->find($leagueId);
        if ( ! $league )
            throw new LeagueNotFoundException;
        $league->delete();

        return "League was successfully deleted";
    }

    /**
     * Adds a user to the a league
     *
     * @param $user
     * @param $leagueId
     * @return array
     * @throws \Exception
     */
    public function addUser( $user, $leagueId )
    {
        if ( ! is_numeric($user) )
        {
            $user = $this->userRepository->getByEmail($user);
            $userEmail = $user['email'];
            $user = $user['id'];
        }
        else
        {
            $user = $this->userRepository->getById($user);
            $userEmail = $user->email;
        }

        $league = $this->getById($leagueId);
        if ( $league->users->find($user) )
            throw new UserInLeagueException('User is already in this league');

        $league->users()->attach($user);

        return "{$userEmail} successfully added to {$league->name}";
    }

    /**
     * Removes a user from a league
     *
     * @param $userId
     * @param $leagueId
     * @return string
     * @throws \Exception
     */
    public function removeUser( $userId, $leagueId )
    {
        $user = $this->userRepository->getById($userId);
        $league = $this->getById($leagueId);
        if ( ! $league->users->find($user) )
            throw new UserNotInLeagueException('User is not in this league');

        $league->users()->detach($user);

        return "{$user->first_name} successfully removed from {$league->name}";
    }

    /**
     * Returns all seasons league has
     *
     * @param $leagueId
     * @return array
     */
    public function getSeasons( $leagueId )
    {
        $league = $this->getById($leagueId);

        $seasons = $league->seasons();
        if ( ! count($seasons) )
            throw new SeasonNotExistsException('League has no seasons');

        return $seasons;
    }

    /**
     * Returns selected season
     *
     * @param $leagueId
     * @param $seasonId
     * @return array
     */
    public function getSeason( $leagueId, $seasonId )
    {
        $league = $this->getById($leagueId);

        $seasons = $league->seasons();
        if ( ! count($seasons) )
            throw new SeasonNotExistsException('League has no seasons');

        foreach ( $seasons as $season )
        {
            if ( $season['season_id'] == $seasonId )
                return $season;
        }

        throw new SeasonNotExistsException('League does not have that season');
    }


    /**
     * Adds a new season to a league
     *
     * @param $leagueId
     * @param $seasonId
     * @return string
     */
    public function addSeason( $leagueId, $seasonId )
    {
        $league = $this->model->find($leagueId);
        if ( ! $league )
            throw new LeagueNotFoundException('Sorry, the league you were looking for does not exists');

        $seasons = $league->seasons();

        foreach ( $seasons as $season )
        {
            if ( $season['season_id'] == $seasonId )
                throw new SeasonAlreadyExistsException('Season already exists');
        }
        $league->addSeason($seasonId);

        return "Season was successfully added to your league";
    }


    /**
     * Removes a season from a league
     *
     * @param $leagueId
     * @param $seasonId
     * @return string
     */
    public function removeSeason( $leagueId, $seasonId )
    {
        $league = $this->model->find($leagueId);
        if ( ! $league )
            throw new LeagueNotFoundException('Sorry, the league you were looking for does not exists');

        $seasons = $league->seasons();

        foreach ( $seasons as $season )
        {
            if ( $season['season_id'] == $seasonId )
            {
                $league->removeSeason($seasonId);

                return "Season was successfully removed from your league";
            }
        }
        throw new SeasonNotExistsException('Season does not exists');
    }
}