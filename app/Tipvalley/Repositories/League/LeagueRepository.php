<?php

namespace App\Tipvalley\Repositories\League;

interface LeagueRepository {

    /**
     * Returns all the leagues
     *
     * @throws \Exception
     * @return array
     */
    public function getAll();

    /**
     * Returns a single league
     *
     * @param $id
     * @throws \Exception
     * @return array
     */
    public function getById( $id );

    /**
     * Creates a new league
     *
     * @param $name
     * @param $description
     * @return string
     */
    public function newLeague($name, $description, $seasonId, array $players);

    /**
     * Updates a single league
     *
     * @param $leagueId
     * @param $name
     * @param $description
     * @throws \Exception
     * @return string
     */
    public function updateLeague( $leagueId, $name, $description);

    /**
     * Deletes a specific league
     *
     * @param $leagueId
     * @throws \Exception
     * @return string
     */
    public function deleteLeague( $leagueId );

    /**
     * Returns all the users for a single league
     *
     * @param $leagueId
     * @throws \Exception
     * @return array
     */
    public function getUsers( $leagueId );

    /**
     * Adds a user to a league
     *
     * @param $userId
     * @param $leagueId
     * @throws \Exception
     * @return string
     */
    public function addUser( $userId, $leagueId );

    /**
     * Removes a user from a league
     *
     * @param $userId
     * @param $leagueId
     * @throws \Exception
     * @return string
     */
    public function removeUser( $userId, $leagueId );

    /**
     * Returns all seasons league has
     *
     * @param $leagueId
     * @return array
     */
    public function getSeasons( $leagueId );

    /**
     * Returns selected season
     *
     * @param $leagueId
     * @param $seasonId
     * @return array
     */
    public function getSeason( $leagueId, $seasonId );

    /**
     * Adds a new season to a league
     *
     * @param $leagueId
     * @param $seasonId
     * @return string
     */
    public function addSeason( $leagueId, $seasonId );


    /**
     * Removes a season from a league
     *
     * @param $leagueId
     * @param $seasonId
     * @return string
     */
    public function removeSeason( $leagueId, $seasonId );
}