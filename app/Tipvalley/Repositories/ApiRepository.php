<?php


namespace App\Tipvalley\Repositories;


abstract class ApiRepository {

    protected $api;

    protected $uri;

    protected $headers;

    /**
     * ApiRepository constructor.
     *
     * @param $api
     */
    public function __construct( $api )
    {
        $this->headers = ['headers' => ['X-Auth-Token' => env('API_KEY')]];
        $this->uri = 'http://api.football-data.org/v1';
        $this->api = $api;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->callApi();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById( $id )
    {
        $this->uri .= "/{$id}";
        return $this->callApi();
    }

    /**
     * Makes a REST get request
     *
     * @return mixed
     */
    protected function callApi()
    {
        $response = $this->api->get($this->uri, $this->headers);
        return json_decode($response->getBody());
    }
}