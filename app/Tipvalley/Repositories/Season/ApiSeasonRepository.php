<?php


namespace App\Tipvalley\Repositories\Season;


use App\Tipvalley\Repositories\ApiRepository;
use GuzzleHttp\Client;

class ApiSeasonRepository extends ApiRepository implements SeasonRepository {

    /**
     * ApiSeasonRepository constructor.
     *
     * @param Client $api
     */
    public function __construct( Client $api )
    {
        parent::__construct($api);
        $this->uri .= "/soccerseasons";
    }
}