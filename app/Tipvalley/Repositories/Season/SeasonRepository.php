<?php

namespace App\Tipvalley\Repositories\Season;

interface SeasonRepository {

    /**
     * Returns all valid seasons
     *
     * @return array
     */
    public function getAll();

    /**
     * Returns a single season
     *
     * @param $id
     * @return array
     */
    public function getById( $id);
}