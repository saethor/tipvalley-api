<?php


namespace App\Tipvalley\Repositories\Fixture;


use App\Tipvalley\Repositories\ApiRepository;
use GuzzleHttp\Client;

class ApiFixtureRepository extends ApiRepository implements FixtureRepository {

    /**
     * ApiFixtureRepository constructor.
     *
     * @param Client $api
     */
    public function __construct( Client $api )
    {
        parent::__construct($api);
        $this->uri .= "/soccerseasons";
        $this->api = $api;
    }

    public function getById( $id )
    {
        $this->uri = 'http://api.football-data.org/v1/fixtures';
        return parent::getById($id);
    }

    /**
     * Returns all fixtures for all seasons in league
     *
     * @param $leagueId
     * @return array
     */
    public function getFixtures( $seasonId )
    {
        $this->uri  .= "/{$seasonId}/fixtures";
        return $this->callApi()->fixtures;
    }

    /**
     * Returns a single fixture
     *
     * @param $leagueId
     * @param $seasonId
     * @return array
     */
    public function getFixture( $leagueId, $seasonId )
    {
        // TODO: Implement getFixture() method.
    }

    /**
     * Stores a tip results
     * Results can only be 1, X, 2
     *
     * @param $fixtureId
     * @param $leagueId
     * @param $results
     * @return String
     */
    public function tip( $fixtureId, $leagueId, $results )
    {

        throw new \Exception('Tip cant be created with this repository');
        // TODO: Implement tip() method.
    }
}