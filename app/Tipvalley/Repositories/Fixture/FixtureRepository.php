<?php

namespace App\Tipvalley\Repositories\Fixture;

interface FixtureRepository {

    /**
     * Returns all fixture for a single season
     *
     * @param $leagueId
     * @param $seasonId
     * @return array
     */
    public function getFixture( $leagueId, $seasonId );

    /**
     * Returns all fixtures for all seasons in league
     *
     * @param $leagueId
     * @return array
     */
    public function getFixtures( $leagueId );

    /**
     * Returns a single fixture
     *
     * @param $id
     * @return array
     */
    public function getById( $id );

    /**
     * Stores a tip results
     * Results can only be 1, X, 2
     *
     * @param $fixtureId
     * @param $leagueId
     * @param $results
     * @return String
     */
    public function tip( $fixtureId, $leagueId, $results);
}