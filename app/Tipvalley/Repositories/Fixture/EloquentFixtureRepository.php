<?php


namespace App\Tipvalley\Repositories\Fixture;


use App\Fixture;
use App\Tipvalley\Exceptions\FixtureIsFinishedException;
use App\Tipvalley\Exceptions\FixtureNotFoundException;
use App\Tipvalley\Exceptions\LeagueNotFoundException;
use App\Tipvalley\Exceptions\SeasonNotExistsException;
use App\Tipvalley\Repositories\EloquentRepository;
use App\Tipvalley\Repositories\League\LeagueRepository;
use App\Tipvalley\Repositories\User\UserRepository;
use App\Tipvalley\Transformers\FixtureTransformer;
use GuzzleHttp\Exception\ClientException;

class EloquentFixtureRepository extends EloquentRepository implements FixtureRepository {

    /**
     * @var FixtureRepository
     */
    private $fixtureRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var FixtureTransformer
     */
    private $transformer;
    /**
     * @var LeagueRepository
     */
    private $leagueRepository;

    /**
     * DbFixtureRepository constructor.
     *
     * @param FixtureRepository $fixtureRepository
     * @param UserRepository $userRepository
     * @param LeagueRepository $leagueRepository
     * @param FixtureTransformer $transformer
     * @param Fixture $model
     */
    public function __construct( FixtureRepository $fixtureRepository, UserRepository $userRepository, LeagueRepository $leagueRepository, FixtureTransformer $transformer, Fixture $model )
    {
        parent::__construct($model);
        $this->model = $model;
        $this->fixtureRepository = $fixtureRepository;
        $this->userRepository = $userRepository;
        $this->transformer = $transformer;
        $this->leagueRepository = $leagueRepository;
    }

    /**
     * Storing a tip for a match
     *
     * @param $fixtureId
     * @param $userId
     * @param $leagueId
     * @param $tipResult
     * @return array
     * @throws \Exception
     */
    public function tip( $fixtureId, $leagueId, $tipResult )
    {
        $user = $this->userRepository->getUser();
        $league = $this->leagueRepository->getById($leagueId);

        if ( ! $league )
            throw new LeagueNotFoundException('League does not exist');

        try
        {
            $fixtureData = $this->fixtureRepository->getById($fixtureId);
        }
        catch (ClientException $e)
        {
            throw new FixtureNotFoundException;
        }

        if ($fixtureData->fixture->result->goalsHomeTeam != null && $fixtureData->fixture->result->goalsAwayTeam != null)
            throw new FixtureIsFinishedException;

        $fixture = array(
            'fixture_id' => $fixtureId,
            'user_id'    => $user->id,
            'league_id'  => $leagueId,
            'results'    => $tipResult
        );

        $tip = $this->model->where('fixture_id', $fixtureId)->where('user_id', $user->id)->get()->first();

        if ( ! $tip )
        {
            $this->model->create($fixture);

            return 'Tip created successfully';
        }
        $tip->update($fixture);

        return 'Tip updated successfully';
    }

    /**
     * Returns all fixtures with a user tips
     *
     * @param $leagueId
     * @return array
     * @throws \Exception
     */
    public function getFixtures( $leagueId )
    {

        $fixtures = array();

        $league = $this->leagueRepository->getById($leagueId);
        if ( ! $league )
            throw new LeagueNotFoundException('League does not exists');

        $seasons = $league->seasons();
        if ( $seasons->isEmpty() )
            throw new SeasonNotExistsException('League does not have any seasons yet');

        $userFixtures = $this->userRepository->getUser()->fixtures->where('league_id', $leagueId)->toArray();

        foreach ( $seasons as $season )
        {

            $apiFixtures = $this->transformer->transformCollection($this->fixtureRepository->getFixtures($season['season_id']));
            $fixtures[ $season['season_id'] ] = $this->arrayMerge($apiFixtures, $userFixtures);
            // $fixtures[ $season['season_id'] ] = $apiFixtures;
        }

        return $fixtures;
    }

    /**
     * Returns all fixture for a single season
     *
     * @param $leagueId
     * @param $seasonId
     * @return array
     */
    public function getFixture( $leagueId, $seasonId )
    {
        $league = $this->leagueRepository->getById($leagueId);
        if ( ! $league )
            throw new LeagueNotFoundException('League does not exists');

        $userFixtures = $this->userRepository->getUser()->fixtures->where('league_id', $leagueId)->toArray();

        $apiFixtures = $this->transformer->transformCollection($this->fixtureRepository->getFixtures($seasonId));

        return $this->arrayMerge($apiFixtures, $userFixtures);
    }

    /**
     * Merge's fixtures from api with a tip from user
     *
     * @param $api
     * @param $user
     * @return mixed
     */
    private function arrayMerge( $api, $user )
    {
        foreach ( $user as $userKey => $userValue)
        {
            foreach ( $api as $apiKey => $apiValue )
                if ( $this->inArrayRecursive($apiValue, $userValue) )
                    $api[ $apiKey ] = array_merge($apiValue, $userValue);
        }


        return $api;
    }

    /**
     * Checks if user has tipped on a match
     *
     * @param $needle
     * @param $heystack
     * @param bool|false $strict
     * @return bool
     */
    private function inArrayRecursive( $needle, $heystack, $strict = false )
    {
        foreach ( $heystack as $item )
        {
            if ( ( $strict ? $item === $needle['fixture_id'] : $item == $needle['fixture_id'] ) || ( is_array($item) && $this->inArrayRecursive($needle, $item, $strict) ) )
            {
                return true;
            }
        }

        return false;
    }
}