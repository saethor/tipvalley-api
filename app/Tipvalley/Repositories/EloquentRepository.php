<?php


namespace App\Tipvalley\Repositories;


abstract class EloquentRepository {

    protected $model;

    /**
     * DbRepository constructor.
     *
     * @param $model
     */
    public function __construct( $model )
    {

        $this->model = $model;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById( $id )
    {
        return $this->model->find($id);
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->model->all();
    }
}