<?php

namespace App\Tipvalley\Repositories\Team;

interface TeamRepository {

    /**
     * Returns a single team
     *
     * @param $id
     * @return array
     */
    public function getById( $id);

    /**
     * Returns all players for a team
     *
     * @param $teamId
     * @return array
     */
    public function getPlayers( $teamId);
}