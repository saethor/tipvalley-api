<?php


namespace App\Tipvalley\Repositories\Team;


use App\Tipvalley\Repositories\ApiRepository;
use GuzzleHttp\Client;

class ApiTeamRepository extends ApiRepository implements TeamRepository {

    /**
     * ApiTeamRepository constructor.
     *
     * @param Client $api
     */
    public function __construct( Client $api)
    {
        parent::__construct($api);
        $this->uri .= "/teams";
        $this->api = $api;
    }

    /**
     * Returns all players for a single team
     *
     * @param $teamId
     * @return array
     */
    public function getPlayers( $teamId)
    {
        $this->uri .= "/{$teamId}/players";
        return $this->callApi()->players;
    }
}