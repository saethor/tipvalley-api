<?php

namespace App\Tipvalley\Repositories\User;

use App\User;

interface UserRepository {
    public function getUser();
    public function create($firstName, $lastName, $email, $password);
    public function update(User $user, $firstName, $lastName, $email, $password);
    public function getLeagues();
    public function getAll();
    public function getById($id);
    public function getByEmail($email);
}