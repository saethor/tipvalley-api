<?php


namespace App\Tipvalley\Repositories\User;


use App\Tipvalley\Exceptions\LeagueNotFoundException;
use App\Tipvalley\Exceptions\UserNotFoundException;
use App\Tipvalley\Repositories\EloquentRepository;
use App\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

class EloquentUserRepository extends EloquentRepository implements UserRepository {

    /**
     * DbUserRepository constructor.
     *
     * @param User $model
     */
    public function __construct( User $model )
    {
        parent::__construct($model);
        $this->model = $model;
    }

    public function create( $firstName, $lastName, $email, $password )
    {
        return $this->model->create([
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email' => $email,
            'password' => bcrypt($password)
        ]);
    }

    public function update( User $user, $firstName, $lastName, $email, $password )
    {
        return $user->update([
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email' => $email,
            'password' => bcrypt($password)
        ]);
    }

    public function getUser()
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate())
            {
                return response()->json(['user_not_found'], 404);
            }
        }
        catch (TokenExpiredException $e)
        {
            return response()->json(['token_expired'], $e->getStatusCode());
        }
        catch (TokenInvalidException $e)
        {
            return response()->json(['token_invalid'], $e->getStatusCode());
        }
        catch (JWTException $e)
        {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        return $user;
    }

    public function getLeagues()
    {
        $user = $this->getUser();
        $leagues = $user->leagues;
        if ( $leagues->isEmpty() )
            throw new LeagueNotFoundException('User not member of any leagues');

        return $leagues->toArray();
    }

    public function getByEmail( $email )
    {
        $user = $this->model->where('email', '=', $email)->first();

        if ( ! $user )
            throw new UserNotFoundException;

        return $user->toArray();
    }
}