<?php


namespace App\Tipvalley\Exceptions;


class SeasonNotExistsException extends \Exception {

    /**
     * SeasonNotExistsException constructor.
     *
     * @param string $string
     */
    public function __construct()
    {}}