<?php


namespace App\Tipvalley\Exceptions;


use Exception;

class UserNotInLeagueException extends Exception {

    /**
     * UserNotInLeagueException constructor.
     *
     * @param string $string
     */
    public function __construct()
    {}}