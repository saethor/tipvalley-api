<?php


namespace App\Tipvalley\Exceptions;


class SeasonAlreadyExistsException extends \Exception {

    /**
     * SeasonAlreadyExistsException constructor.
     *
     * @param string $string
     */
    public function __construct()
    {}}