<?php


namespace App\Tipvalley\Exceptions;


use Exception;

class UserInLeagueException extends Exception {

    /**
     * UserInLeagueException constructor.
     *
     * @param string $string
     */
    public function __construct()
    {}}