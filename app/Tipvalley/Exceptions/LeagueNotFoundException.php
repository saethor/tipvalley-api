<?php


namespace App\Tipvalley\Exceptions;


use Exception;

class LeagueNotFoundException extends Exception {

    /**
     * LeagueNotFoundException constructor.
     *
     * @param string $string
     */
    public function __construct( )
    {}}