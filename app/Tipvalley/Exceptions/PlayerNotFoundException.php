<?php


namespace App\Tipvalley\Exceptions;


use Exception;

class PlayerNotFoundException extends Exception {

    /**
     * PlayerNotFoundException constructor.
     */
    public function __construct()
    {
    }
}