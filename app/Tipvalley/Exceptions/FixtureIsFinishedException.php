<?php


namespace App\Tipvalley\Exceptions;


use Exception;

class FixtureIsFinishedException extends Exception {

    /**
     * FixtureIsFinished constructor.
     */
    public function __construct()
    {
    }
}