<?php


namespace App\Tipvalley\Exceptions;


use Exception;

class FixtureNotFoundException extends Exception {

    /**
     * fixtureNotFoundException constructor.
     */
    public function __construct()
    {
    }
}