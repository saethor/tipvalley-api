<?php


namespace App\Tipvalley\Exceptions;


class UserNotFoundException extends \Exception {

    /**
     * UserNotFoundException constructor.
     */
    public function __construct()
    {
    }
}