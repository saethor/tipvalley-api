<?php


namespace App\Tipvalley\Transformers;


class PlayersTransformer extends Transformer{

    /**
     * @param $item
     * @return mixed
     */
    public function transform( $player )
    {
        return [
            'id' => $player->id,
            'name' => $player->name,
            'position' => $player->position,
            'jerseyNumber' => $player->jerseyNumber,
            'dateOfBirth' => $player->dateOfBirth,
            'nationality' => $player->nationality,
            'contractUntil' => $player->contractUntil,
            'marketValue' => $player->marketValue,
        ];
    }
}