<?php


namespace App\Tipvalley\Transformers;


class TeamTransformer extends Transformer{

    /**
     * @param $item
     * @return mixed
     */
    public function transform( $team )
    {
        return [
            'name' => $team->name,
            'code' => $team->code,
            'shortName' => $team->shortName,
            'squadMarketValue' => $team->squadMarketValue,
            'crestUrl' => $team->crestUrl
        ];
    }
}