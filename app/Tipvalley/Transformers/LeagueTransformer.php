<?php


namespace App\Tipvalley\Transformers;


use App\League;

class LeagueTransformer extends Transformer {

    /**
     * @param $league
     * @return mixed
     * @internal param $item
     */
    public function transform( $league )
    {
        $users = League::find($league['id'])->users;

        return [
            'id'          => $league['id'],
            'name'        => $league['name'],
            'description' => $league['description'],
            'users'       => $this->transformUsers($users)
        ];
    }


    private function transformUsers( $users )
    {
        $usersArray = array();
        foreach ( $users as $user )
        {
            array_push($usersArray, (new UserTransformer())->transform($user));
        }
        return $usersArray;
    }
}