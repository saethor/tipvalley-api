<?php


namespace App\Tipvalley\Transformers;


class SeasonTransformer extends Transformer {

    /**
     * @param $item
     * @return mixed
     */
    public function transform( $season )
    {
        $path = explode('/', $season->_links->self->href);
        $id = $path[ count($path) - 1];
        return [
            'id'            => $id,
            'caption'       => $season->caption,
            'league'        => $season->league,
            'year'          => $season->year,
            'numberOfTeams' => $season->numberOfTeams,
            'numberOfGames' => $season->numberOfGames,
            'lastUpdated'   => $season->lastUpdated
        ];
    }
}