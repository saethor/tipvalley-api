<?php


namespace App\Tipvalley\Transformers;


class UserTransformer extends Transformer{

    /**
     * @param $item
     * @return mixed
     */
    public function transform( $user )
    {
        return [
            'id' => $user['id'],
            'name' => $user['first_name'] . ' ' . $user['last_name'],
            'email' => $user['email']
        ];
    }
}