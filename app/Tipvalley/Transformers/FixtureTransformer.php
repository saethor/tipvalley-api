<?php


namespace App\Tipvalley\Transformers;


class FixtureTransformer extends Transformer {

    /**
     * @param $item
     * @return mixed
     */
    public function transform( $fixture )
    {
        $path = explode('/', $fixture->_links->self->href);
        $id = $path[ count($path) - 1];
        return [
            'fixture_id' => $id,
            'date' => $fixture->date,
            'status' => $fixture->status,
            'matchday' => $fixture->matchday,
            'homeTeamName' => $fixture->homeTeamName,
            'awayTeamName' => $fixture->awayTeamName,
            'result' => [
                'goalsHomeTeam' => $fixture->result->goalsHomeTeam,
                'goalsAwayTeam' => $fixture->result->goalsAwayTeam
            ]

        ];
    }
}