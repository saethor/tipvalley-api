<?php

namespace App\Providers;

use App\Fixture;
use App\League;
use App\Tipvalley\Repositories\Fixture\EloquentFixtureRepository;
use App\Tipvalley\Transformers\FixtureTransformer;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;
use App\Tipvalley\Repositories\League\LeagueRepository;
use App\Tipvalley\Repositories\League\EloquentLeagueRepository;
use App\Tipvalley\Repositories\Season\SeasonRepository;
use App\Tipvalley\Repositories\Season\ApiSeasonRepository;
use App\Tipvalley\Repositories\Team\TeamRepository;
use App\Tipvalley\Repositories\Team\ApiTeamRepository;
use App\Tipvalley\Repositories\User\UserRepository;
use App\Tipvalley\Repositories\User\EloquentUserRepository;
use App\Tipvalley\Repositories\Fixture\FixtureRepository;
use App\Tipvalley\Repositories\Fixture\ApiFixtureRepository;


class DatabaseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SeasonRepository::class, ApiSeasonRepository::class);
        $this->app->bind(LeagueRepository::class, EloquentLeagueRepository::class);
        $this->app->bind(TeamRepository::class, ApiTeamRepository::class);
        $this->app->bind(UserRepository::class, EloquentUserRepository::class);
        $this->app->singleton(FixtureRepository::class, function ()
        {
            return new EloquentFixtureRepository(
                new ApiFixtureRepository(new Client),
                new EloquentUserRepository(new User),
                new EloquentLeagueRepository(new EloquentUserRepository(new User), new League),
                new FixtureTransformer,
                new Fixture
            );
        });
    }
}
