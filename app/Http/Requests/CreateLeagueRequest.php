<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateLeagueRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Validation for storing a new league.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|min:3',
            'description' => 'required',
            'season_id' => 'required|numeric',
            'players'   => 'array',
        ];
    }
}
