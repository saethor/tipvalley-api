<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Team;

Route::group(['prefix' => '/beta'], function ()
{
    Route::resource('leagues', 'LeaguesController', ['except' => ['create', 'edit']]);
    Route::resource('leagues.users', 'LeaguesUsersController', ['except' => ['create', 'edit']]);
    Route::resource('leagues.fixtures', 'FixturesController', ['only' => ['index', 'store']]);
    Route::resource('leagues.seasons', 'LeaguesSeasonsController', ['except' => ['create', 'update', 'edit']]);

    Route::resource('seasons', 'SeasonsController', ['only' => ['index', 'show']]);

    Route::resource('users', 'UsersController', ['except' => ['create', 'edit']]);
    Route::post('users/authenticate', 'UsersController@authenticate');
    Route::resource('teams', 'TeamsController', ['only' => ['show']]);
    Route::resource('teams.players', 'PlayersController', ['only' => ['index']]);


});

Route::any('{all}', function($uri)
{
    return response()->json(['error' => 'Could not find what you were looking for', 'status_code' => 404], 404);
})->where('all', '.*');