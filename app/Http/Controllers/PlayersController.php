<?php

namespace App\Http\Controllers;

use App\Tipvalley\Repositories\Team\TeamRepository;
use App\Tipvalley\Transformers\PlayersTransformer;
use App\Http\Requests;

class PlayersController extends ApiController
{

    /**
     * @var PlayersTransformer
     */
    private $playersTransformer;
    /**
     * @var TeamRepository
     */
    private $repository;

    /**
     * PlayersController constructor.
     *
     * @param TeamRepository $repository
     * @param PlayersTransformer $playersTransformer
     */
    public function __construct( TeamRepository $repository , PlayersTransformer $playersTransformer )
    {
        $this->middleware('jwt.auth');
        $this->playersTransformer = $playersTransformer;
        $this->repository = $repository;
    }

    /**
     * Display a listing of the player.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        if ( ! is_numeric($id) )
        {
            return $this->respondeNotFound();
        }
        $players = $this->repository->getPlayers($id);

        return $this->responde([
            'players' => $this->playersTransformer->transformCollection($players)
        ]);
    }
}
