<?php

namespace App\Http\Controllers;

use app\Tipvalley\Exceptions\UserInLeagueException;
use app\Tipvalley\Exceptions\UserNotInLeagueException;
use App\Tipvalley\Repositories\League\LeagueRepository;
use App\Tipvalley\Transformers\UserTransformer;
use Illuminate\Http\Request;
use App\Http\Requests;

class LeaguesUsersController extends ApiController {

    /**
     * @var UserTransformer
     */
    private $userTransformer;

    /**
     * @var LeagueRepository
     */
    private $repository;

    /**
     * LeaguesUsersController constructor.
     *
     * @param LeagueRepository $repository
     * @param UserTransformer $userTransformer
     */
    public function __construct( LeagueRepository $repository, UserTransformer $userTransformer )
    {
        $this->middleware('jwt.auth');
        $this->userTransformer = $userTransformer;
        $this->repository = $repository;
    }

    /**
     * Display a listing of all the leagues for authenticated user.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function index( $id )
    {
        try
        {
            $users = $this->repository->getUsers((int) $id);

            return $this->responde([
                'users' => $this->userTransformer->transformCollection($users)
            ]);
        }
        catch (\Exception $e)
        {
            return $this->respondeNotFound($e->getMessage());
        }

    }

    /**
     * Adds a new user to a league
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request, $id )
    {
        try
        {
            return $this->respondeContentCreated(
                $this->repository->addUser($request->get('user'), (int) $id)
            );
        }
        catch (UserInLeagueException $e)
        {
            return $this->respondeAlreadyExists('User is already in this league');
        }
    }

    /**
     * Updates a user to a admin
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        // TODO: Make user a admin
    }

    /**
     * Removes a user from a league
     *
     * @param $leagueId
     * @param $userId
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy( $leagueId, $userId )
    {
        try
        {
            return $this->respondeWithMessage(
                $this->repository->removeUser( (int) $userId, (int) $leagueId)
            );
        }
        catch (UserNotInLeagueException $e)
        {
            return $this->respondeNotFound($e->getMessage());
        }
    }
}
