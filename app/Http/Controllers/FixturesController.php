<?php

namespace App\Http\Controllers;

use App\Fixture;
use App\Http\Requests\StoreFixtureRequest;
use App\Tipvalley\Exceptions\FixtureIsFinishedException;
use App\Tipvalley\Exceptions\FixtureNotFoundException;
use app\Tipvalley\Exceptions\LeagueNotFoundException;
use app\Tipvalley\Exceptions\SeasonNotExistsException;
use App\Tipvalley\Repositories\Fixture\FixtureRepository;
use App\Tipvalley\Repositories\League\LeagueRepository;
use App\Tipvalley\Transformers\FixtureTransformer;
use Illuminate\Http\Request;
use App\Http\Requests;

class FixturesController extends ApiController {

    /**
     * @var FixtureTransformer
     */
    private $fixtureTransformer;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var Fixture
     */
    private $fixture;
    /**
     * @var FixtureRepository
     */
    private $fixtureRepository;
    /**
     * @var LeagueRepository
     */
    private $leagueRepository;


    /**
     * FixturesController constructor.
     *
     * @param FixtureRepository $fixtureRepository
     * @param LeagueRepository $leagueRepository
     * @param FixtureTransformer $fixtureTransformer
     * @param Request $request
     * @param Fixture $fixture
     */
    public function __construct( FixtureRepository $fixtureRepository, LeagueRepository $leagueRepository, FixtureTransformer $fixtureTransformer, Request $request, Fixture $fixture )
    {
        $this->middleware('jwt.auth');
        $this->fixtureTransformer = $fixtureTransformer;
        $this->request = $request;
        $this->fixture = $fixture;
        $this->fixtureRepository = $fixtureRepository;
        $this->leagueRepository = $leagueRepository;
    }

    /**
     * Display a listing of fixtures.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function index( $id )
    {
        try
        {
            return $this->responde($this->fixtureRepository->getFixtures((int) $id));

        } catch ( LeagueNotFoundException $e )
        {
            return $this->respondeNotFound($e->getMessage());
        } catch ( SeasonNotExistsException $e )
        {
            return $this->respondeNotFound($e->getMessage());
        }
    }


    /**
     * Store a newly created tip in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function store( StoreFixtureRequest $request, $id )
    {
        try
        {
            return $this->respondeWithMessage(
                $this->fixtureRepository->tip((int) $request->get('fixture_id'), (int) $id, $request->get('results'))
            );
        } catch ( LeagueNotFoundException $e )
        {
            return $this->respondeWithError($e->getMessage());
        } catch ( FixtureNotFoundException $e )
        {
            return $this->respondeWithError('This fixture does not exists');
        } catch ( FixtureIsFinishedException $e )
        {
            return $this->respondeWithError('This fixture is finished');
        }

    }

    /**
     * Display the specified fixture.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
        // TODO: Show single fixture with head to head;
    }
}
