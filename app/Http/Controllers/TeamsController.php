<?php

namespace App\Http\Controllers;

use App\Tipvalley\Repositories\Team\TeamRepository;
use App\Tipvalley\Transformers\TeamTransformer;
use App\Http\Requests;

class TeamsController extends ApiController
{

    /**
     * @var TeamTransformer
     */
    private $teamTransformer;
    /**
     * @var TeamRepository
     */
    private $repository;

    /**
     * TeamsController constructor.
     *
     * @param TeamRepository $repository
     * @param TeamTransformer $teamTransformer
     */
    public function __construct( TeamRepository $repository, TeamTransformer $teamTransformer )
    {
        $this->middleware('jwt.auth');
        $this->teamTransformer = $teamTransformer;
        $this->repository = $repository;
    }
    /**
     * Display the specified team.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $team = $this->repository->getById($id);
        return $this->responde([
            'team' => $this->teamTransformer->transform($team)
        ]);
    }
}
