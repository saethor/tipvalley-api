<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLeagueRequest;
use App\Tipvalley\Exceptions\LeagueNotFoundException;
use App\Tipvalley\Exceptions\SeasonNotExistsException;
use App\Tipvalley\Repositories\League\LeagueRepository;
use App\Tipvalley\Repositories\User\UserRepository;
use App\Tipvalley\Transformers\LeagueTransformer;
use Illuminate\Http\Request;
use App\Http\Requests;

class LeaguesController extends ApiController
{

    /**
     * @var LeagueTransformer
     */
    private $leagueTransformer;

    /**
     * @var LeagueRepository
     */
    private $leagueRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param LeagueTransformer $leagueTransformer
     * @param UserRepository $userRepository
     * @param LeagueRepository $leagueRepository
     */
    public function __construct( LeagueTransformer $leagueTransformer, UserRepository $userRepository, LeagueRepository $leagueRepository )
    {
        $this->middleware('jwt.auth');
        $this->leagueTransformer = $leagueTransformer;
        $this->leagueRepository = $leagueRepository;
        $this->userRepository = $userRepository;
    }
    /**
     * Display a listing of the all the leagues a authenticated user is a member of.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try
        {
            $leagues = $this->userRepository->getLeagues();
            return $this->responde([
                'leagues' => $this->leagueTransformer->transformCollection($leagues)
            ]);
        }
        catch (LeagueNotFoundException $e)
        {
            return $this->respondeNotFound($e->getMessage());
        }
    }

    /**
     * Store a newly created league in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( CreateLeagueRequest $request)
    {
        try
        {
            $message = $this->leagueRepository->newLeague($request->get('name'), $request->get('description'), $request->get('season_id'), $request->get('players'));

            return $this->respondeContentCreated($message);
        }
        catch ( SeasonNotExistsException $e )
        {
            return $this->respondeWithError($e->getMessage());
        }

    }

    /**
     * Display the specified league.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $league = $this->leagueRepository->getById($id);

            return $this->responde([
                'league' => $this->leagueTransformer->transform($league)
            ]);

        }
        catch (LeagueNotFoundException $e)
        {
            return $this->respondeNotFound($e->getMessage());
        }


    }

    /**
     * Update the specified league in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       try
       {
           return $this->respondeWithMessage(
               $this->leagueRepository->updateLeague((int) $id, $request->get('name'), $request->get('description'))
           );
       }
       catch (LeagueNotFoundException $e)
       {
           return $this->respondeNotFound($e->getMessage());
       }
    }

    /**
     * Remove the specified league from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            return $this->respondeWithMessage(
                $this->leagueRepository->deleteLeague($id)
            );
        }
        catch (LeagueNotFoundException $e)
        {
            return $this->respondeNotFound($e->getMessage());
        }
    }
}
