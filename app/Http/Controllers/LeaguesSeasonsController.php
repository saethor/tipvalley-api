<?php

namespace App\Http\Controllers;

use app\Tipvalley\Exceptions\LeagueNotFoundException;
use app\Tipvalley\Exceptions\SeasonAlreadyExistsException;
use app\Tipvalley\Exceptions\SeasonNotExistsException;
use App\Tipvalley\Repositories\League\LeagueRepository;
use App\Tipvalley\Repositories\Season\SeasonRepository;
use App\Tipvalley\Transformers\SeasonTransformer;
use Illuminate\Http\Request;
use App\Http\Requests;

class LeaguesSeasonsController extends ApiController {

    /**
     * @var SeasonRepository
     */
    private $seasonRepository;
    /**
     * @var LeagueRepository
     */
    private $leagueRepository;
    /**
     * @var SeasonTransformer
     */
    private $transformer;

    /**
     * LeaguesSeasonsController constructor.
     *
     * @param SeasonRepository $seasonRepository
     * @param LeagueRepository $leagueRepository
     * @param SeasonTransformer $transformer
     */
    public function __construct( SeasonRepository $seasonRepository, LeagueRepository $leagueRepository, SeasonTransformer $transformer )
    {
        $this->middleware('jwt.auth');
        $this->seasonRepository = $seasonRepository;
        $this->leagueRepository = $leagueRepository;
        $this->transformer = $transformer;
    }

    /**
     * Display a listing of all the season in league.
     *
     * @param $leagueId
     * @return \Illuminate\Http\Response
     */
    public function index( $leagueId )
    {
        try
        {
            return $this->responde([
                'seasons' => $this->leagueRepository->getSeasons($leagueId)
            ]);
        }
        catch (\Exception $e)
        {
            return $this->respondeNotFound($e->getMessage());
        }
    }

    /**
     * Adds a new season to a league
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request, $id)
    {
        try
        {
            return $this->respondeContentCreated($this->leagueRepository->addSeason($id, $request->get('season_id')));
        }
        catch (LeagueNotFoundException $e)
        {
            return $this->respondeNotFound($e->getMessage());
        }
        catch ( SeasonAlreadyExistsException $e )
        {
            return $this->respondeWithError($e->getMessage());
        }
    }

    /**
     * Display the specified season in league.
     *
     * @param  int $id
     * @param $seasonId
     * @return \Illuminate\Http\Response
     */
    public function show( $id, $seasonId )
    {
        try
        {
             return $this->responde(
                 $this->leagueRepository->getSeason((int) $id, (int) $seasonId )
             );
        }
        catch (SeasonNotExistsException $e)
        {
            return $this->respondeNotFound($e->getMessage());
        }
    }

    /**
     * Remove the specified season from league.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id, $seasonId )
    {
        try
        {
            return $this->respondeWithMessage(
                $this->leagueRepository->removeSeason((int) $id, (int) $seasonId)
            );

        }
        catch (LeagueNotFoundException $e)
        {
            return $this->respondeNotFound($e->getMessage());
        }
    }
}
