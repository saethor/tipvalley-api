<?php

namespace App\Http\Controllers;

use App\Tipvalley\Repositories\Season\SeasonRepository;
use App\Tipvalley\Transformers\SeasonTransformer;


use App\Http\Requests;

class SeasonsController extends ApiController {

    /**
     * @var SeasonTransformer
     */
    private $sessionTransformer;
    /**
     * @var SeasonRepository
     */
    private $repository;


    /**
     * SeasonsController constructor.
     *
     * @param SeasonTransformer $sessionTransformer
     * @param SeasonRepository $repository
     */
    public function __construct( SeasonTransformer $sessionTransformer, SeasonRepository $repository )
    {
        $this->middleware('jwt.auth');
        $this->sessionTransformer = $sessionTransformer;
        $this->repository = $repository;
    }
    /**
     * Display a listing of the seasons.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seasons = $this->repository->getAll();
        return $this->responde([
            'seasons' => $this->sessionTransformer->transformCollection($seasons)
        ]);
    }

    /**
     * Display the specified season.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
        $season = $this->repository->getById($id);
        return $this->responde([
            'season' => $this->sessionTransformer->transform($season)
        ]);
    }
}
