<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Tipvalley\Repositories\User\UserRepository;
use App\Tipvalley\Transformers\UserTransformer;
use Illuminate\Http\Request;
use App\Http\Requests;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class UsersController extends ApiController {

    /**
     * @var UserTransformer
     */
    private $userTransformer;

    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * UsersController constructor.
     *
     * @param UserRepository $repository
     * @param UserTransformer $userTransformer
     */
    public function __construct( UserRepository $repository, UserTransformer $userTransformer )
    {
        $this->middleware('jwt.auth', ['except' => ['store', 'authenticate']]);
        $this->userTransformer = $userTransformer;
        $this->repository = $repository;
    }

    /**
     * Display a authenticated user
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->repository->getUser();

        return $this->responde([
            'user' => $this->userTransformer->transform($user)
        ]);
    }

    /**
     * Store a newly created user in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store( UserRequest $request )
    {
        $user = $this->repository->create(
            $request->get('first_name'),
            $request->get('last_name'),
            $request->get('email'),
            $request->get('password')
        );

        return $this->respondeWithMessage("{$user->email} successfully created");
    }

    /**
     * Display the specified user.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
        $user = $this->repository->getById($id);
        if ( ! $user )
        {
            return $this->respondeNotFound('This user does not exists');
        }

        return $this->responde([
            'user' => $this->userTransformer->transform($user)
        ]);
    }

    /**
     * Update the specified user in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update( UserRequest $request, $id )
    {
        $user = $this->repository->getById($id);
        if ( ! $user )
        {
            return $this->respondeNotFound('This user does not exists');
        }
        $this->repository->update(
            $user,
            $request->get('first_name'),
            $request->get('last_name'),
            $request->get('email'),
            bcrypt($request->get('password'))
        );

        return $this->respondeWithMessage("{$user->email} was successfully updated");
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        $user = $this->repository->getById($id);
        if ( ! $user )
        {
            return $this->respondeNotFound('This user does not exists');
        }
        $user['active'] = 0;
        $user->save();

        return $this->respondeWithMessage("{$user->email} was successfully deleted");
    }

    /**
     * Generate a new JWT Token for a user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate( Request $request )
    {
        $credentials = $request->only('email', 'password');

        try
        {
            // Verify the credentials and create a token for the user
            if ( ! $token = JWTAuth::attempt($credentials) )
            {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch ( JWTException $e )
        {
            // Something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // If no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }
}
