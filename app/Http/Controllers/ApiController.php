<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response as IlluminateResponse;
use App\Http\Requests;

abstract class ApiController extends Controller {

    /**
     * @var integer
     */
    protected $statusCode = 200;

    /**
     * Gets the value of statusCode.
     *
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Sets the value of statusCode.
     *
     * @param mixed $statusCode the status code
     *
     * @return self
     */
    public function setStatusCode( $statusCode )
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * For a basic responde
     *
     * @param $data
     * @param array $headers
     * @return mixed
     */
    public function responde( $data, $headers = [] )
    {
        return response()->json($data, $this->getStatusCode(), $headers);
    }

    /**
     * When responding with a error
     *
     * @param $message
     * @return mixed
     */
    public function respondeWithError( $message )
    {
        return $this->responde(['error' => $message, 'status_code' => $this->getStatusCode()]);
    }

    /**
     * When returning a message and a status code
     *
     * @param $message
     * @return mixed
     */
    public function respondeWithMessage( $message )
    {
        return $this->responde(['message' => $message, 'status_code' => $this->getStatusCode()]);
    }

    /**
     * Used when content already exists
     *
     * @param $message
     * @return mixed
     */
    public function respondeAlreadyExists( $message )
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_CONFLICT)->respondeWithMessage($message);
    }

    /**
     * Used when no content is returned in response
     *
     * @param string $message
     * @return mixed
     */
    public function respondeNoContent( $message = 'No content' )
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_NO_CONTENT)->respondeWithMessage($message);
    }

    /**
     * When content is created
     *
     * @param $message
     * @return mixed
     */
    public function respondeContentCreated( $message )
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_CREATED)->respondeWithMessage($message);
    }

    /**
     * If no content is found
     *
     * @param string $message
     * @return mixed
     */
    public function respondeNotFound( $message = 'Not found!' )
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_NOT_FOUND)->respondeWithError($message);
    }
}
