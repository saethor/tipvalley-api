<?php

use GuzzleHttp\Client;
use Illuminate\Database\Seeder;

class SeasonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new Client();
        $uri = 'http://api.football-data.org/alpha/soccerseasons/';
        $headers = ['headers' => ['X-Auth-Token' => '8e18a2264c574b73b02e9c68403bcd29']];
        $response = $client->get($uri, $headers); 
        $seasons = json_decode($response->getBody());

        foreach ( $seasons as $season )
        {
            $data = [
                substr($season->_links->self->href, -3),
                $season->caption,
                $season->league,
                $season->year
            ];
            
            DB::select('CALL CreateSeason(?, ?, ?, ?)', $data);
        }
    }
}
