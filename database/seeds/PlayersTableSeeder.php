<?php

use App\Team;
use GuzzleHttp\Client;
use Illuminate\Database\Seeder;

class PlayersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $season = App\Season::find(398);
        // $teams = Team::all();
        $teams = $season->teams;
        foreach ( $teams as $team )
        {
            try
            {
                $client = new Client();
                $uri = "http://api.football-data.org/alpha/teams/{$team->id}/players";
                $headers = ['headers' => ['X-Auth-Token' => '8e18a2264c574b73b02e9c68403bcd29']];
                $response = $client->get($uri, $headers);
                $players = json_decode($response->getBody());


                foreach ( $players->players as $player )
                {
                    $data = [
                        $player->id,
                        $player->name,
                        $player->position,
                        $player->jerseyNumber,
                        $player->dateOfBirth,
                        $player->nationality,
                        $player->contractUntil,
                        (int) str_replace(',', '', substr($player->marketValue, 0, - 4))
                    ];

                    DB::select('CALL CreatePlayer(?,?,?,?,?,?,?,?)', $data);
                    DB::select('CALL AddPlayerToTeam(?,?)', array($player->id, $team->id));
                }
            } catch ( \GuzzleHttp\Exception\ClientException $e )
            {
                echo $e->getMessage();
            }
        }
    }
}
