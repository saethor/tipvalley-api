<?php

use App\Fixture;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Seeder;

class FixturesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new Client();
        $uri = 'http://api.football-data.org/alpha/soccerseasons/398/fixtures';
        $headers = ['headers' => ['X-Auth-Token' => '8e18a2264c574b73b02e9c68403bcd29']];
        $response = $client->get($uri, $headers);
        $fixtures = json_decode($response->getBody());
        $seasonPath = explode('/', $fixtures->_links[1]->soccerseason);
        $seasonId = $seasonPath[ count($seasonPath) - 1 ];

        foreach ( $fixtures->fixtures as $fixture )
        {
            $path = explode('/', $fixture->_links->self->href);
            $id = $path[ count($path) - 1 ];
            $date = Carbon::parse($fixture->date);
            $homeTeamID = DB::select("CALL GetTeamId(?)", array($fixture->homeTeamName))[0];
            $awayTeamID = DB::select('CALL GetTeamId(?)', array($fixture->awayTeamName))[0];
            $data = [
                'id'            => $id,
                'date'          => $date->format('Y-m-d H:m:s'),
                'status'        => $fixture->status,
                'matchday'      => $fixture->matchday,
                'homeTeamID'    => $homeTeamID->id,
                'awayTeamID'    => $awayTeamID->id,
                'goalsHomeTeam' => $fixture->result->goalsHomeTeam,
                'goalsAwayTeam' => $fixture->result->goalsAwayTeam,
                'seasonID'      => $seasonId,
            ];
            Fixture::create($data);
        }
    }
}
