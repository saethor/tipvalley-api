<?php

use GuzzleHttp\Client;
use Illuminate\Database\Seeder;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seasons = DB::select('call ReadSeasons()');

        foreach ($seasons as $season)
        {
            $client = new GuzzleHttp\Client();
            $uri = "http://api.football-data.org/alpha/soccerseasons/{$season->id}/teams";
            $headers = ['headers' => ['X-Auth-Token' => '8e18a2264c574b73b02e9c68403bcd29']];
            $response = $client->get($uri, $headers); 
            $teams = json_decode($response->getBody());

            foreach ($teams->teams as $team) {
                $path = explode('/', $team->_links->self->href);
                $id = $path[ count($path) - 1];

                $data = [
                    $id,
                    $team->name,
                    $team->code,
                    $team->shortName,
                    $team->crestUrl
                ];

                DB::select(' CreateTeam(?,?,?,?,?)', $data);
                DB::select('CALL AddTeamToSeason(?,?)', array($season->id, $id));
            }
        }
    }
}
