var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var paths = {
    'bower': './bower_components/',
    'node': '../'
}

elixir(function(mix) {
    mix.sass([
        'app.scss'
    ])
        .scripts([
            '../../../node_modules/jquery/dist/jquery.js',
            '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
            '../../../node_modules/angular/angular.js',
            '../../../node_modules/angular-ui-router/build/angular-ui-router.js',
            '../../../node_modules/satellizer/satellizer.js',
            'app.js',
            'authController.js',
            'userController.js'
        ])

});
