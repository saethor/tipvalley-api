#Tip Valley API


##Endpoints
| Method   | URI                                      | Name                         | Action                                                | Middleware |
|----------|------------------------------------------|------------------------------|-------------------------------------------------------|------------|
| GET|HEAD | beta/leagues                             | beta.leagues.index           | App\Http\Controllers\LeaguesController@index          | jwt.auth   |
| POST     | beta/leagues                             | beta.leagues.store           | App\Http\Controllers\LeaguesController@store          | jwt.auth   |
| DELETE   | beta/leagues/{leagues}                   | beta.leagues.destroy         | App\Http\Controllers\LeaguesController@destroy        | jwt.auth   |
| PATCH    | beta/leagues/{leagues}                   |                              | App\Http\Controllers\LeaguesController@update         | jwt.auth   |
| GET|HEAD | beta/leagues/{leagues}                   | beta.leagues.show            | App\Http\Controllers\LeaguesController@show           | jwt.auth   |
| PUT      | beta/leagues/{leagues}                   | beta.leagues.update          | App\Http\Controllers\LeaguesController@update         | jwt.auth   |
| POST     | beta/leagues/{leagues}/fixtures          | beta.leagues.fixtures.store  | App\Http\Controllers\FixturesController@store         | jwt.auth   |
| GET|HEAD | beta/leagues/{leagues}/fixtures          | beta.leagues.fixtures.index  | App\Http\Controllers\FixturesController@index         | jwt.auth   |
| POST     | beta/leagues/{leagues}/seasons           | beta.leagues.seasons.store   | App\Http\Controllers\LeaguesSeasonsController@store   | jwt.auth   |
| GET|HEAD | beta/leagues/{leagues}/seasons           | beta.leagues.seasons.index   | App\Http\Controllers\LeaguesSeasonsController@index   | jwt.auth   |
| GET|HEAD | beta/leagues/{leagues}/seasons/{seasons} | beta.leagues.seasons.show    | App\Http\Controllers\LeaguesSeasonsController@show    | jwt.auth   |
| DELETE   | beta/leagues/{leagues}/seasons/{seasons} | beta.leagues.seasons.destroy | App\Http\Controllers\LeaguesSeasonsController@destroy | jwt.auth   |
| GET|HEAD | beta/leagues/{leagues}/users             | beta.leagues.users.index     | App\Http\Controllers\LeaguesUsersController@index     | jwt.auth   |
| POST     | beta/leagues/{leagues}/users             | beta.leagues.users.store     | App\Http\Controllers\LeaguesUsersController@store     | jwt.auth   |
| DELETE   | beta/leagues/{leagues}/users/{users}     | beta.leagues.users.destroy   | App\Http\Controllers\LeaguesUsersController@destroy   | jwt.auth   |
| PATCH    | beta/leagues/{leagues}/users/{users}     |                              | App\Http\Controllers\LeaguesUsersController@update    | jwt.auth   |
| PUT      | beta/leagues/{leagues}/users/{users}     | beta.leagues.users.update    | App\Http\Controllers\LeaguesUsersController@update    | jwt.auth   |
| GET|HEAD | beta/leagues/{leagues}/users/{users}     | beta.leagues.users.show      | App\Http\Controllers\LeaguesUsersController@show      | jwt.auth   |
| GET|HEAD | beta/seasons                             | beta.seasons.index           | App\Http\Controllers\SeasonsController@index          | jwt.auth   |
| GET|HEAD | beta/seasons/{seasons}                   | beta.seasons.show            | App\Http\Controllers\SeasonsController@show           | jwt.auth   |
| GET|HEAD | beta/teams/{teams}                       | beta.teams.show              | App\Http\Controllers\TeamsController@show             | jwt.auth   |
| GET|HEAD | beta/teams/{teams}/players               | beta.teams.players.index     | App\Http\Controllers\PlayersController@index          | jwt.auth   |
| GET|HEAD | beta/users                               | beta.users.index             | App\Http\Controllers\UsersController@index            | jwt.auth   |
| POST     | beta/users                               | beta.users.store             | App\Http\Controllers\UsersController@store            |            |
| POST     | beta/users/authenticate                  |                              | App\Http\Controllers\UsersController@authenticate     |            |
| PATCH    | beta/users/{users}                       |                              | App\Http\Controllers\UsersController@update           | jwt.auth   |
| GET|HEAD | beta/users/{users}                       | beta.users.show              | App\Http\Controllers\UsersController@show             | jwt.auth   |
| PUT      | beta/users/{users}                       | beta.users.update            | App\Http\Controllers\UsersController@update           | jwt.auth   |
| DELETE   | beta/users/{users}                       | beta.users.destroy           | App\Http\Controllers\UsersController@destroy          | jwt.auth   |
